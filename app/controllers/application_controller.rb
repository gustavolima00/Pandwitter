class ApplicationController < ActionController::Base
  include Pundit
  #protected_from_forgery with: :exception
  
  def after_sign_in_path_for(resource_or_scope)
    if current_user.profile.length == 0
      "/profiles/new"
    else
      "/posts"
    end
  end

rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
private
  def user_not_authorized
    flash[:alert] = "Você não tem permissão para fazer essa ação."
    redirect_to(request.referrer || root_path)
  end
end
